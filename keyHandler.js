let exec = require("child_process").exec,
  config = require("./config.js"),
  lastTime = {},
  windowID = "unfilled",
  throttledCommands = config.throttledCommands,
  regexThrottle = new RegExp("^(" + throttledCommands.join("|") + ")$", "i"),
  regexFilter = new RegExp(
    "^(" + config.filteredCommands.join("|") + ")$",
    "i"
  );

let isWindows = process.platform === "win32";

(function setWindowID() {
  if (!isWindows & windowID === "unfilled") {
    exec("xdotool search --onlyvisible --name " + config.programName, function (
      error,
      stdout
    ) {
      windowID = stdout.trim();
      // console.log(key, windowID);
    });
  }
})();

for (let i = 0; i < throttledCommands.length; i++) {
  lastTime[throttledCommands[i]] = new Date().getTime();
}

let defaultKeyMap = config.keymap || {
  up: "up",
  left: "left",
  down: "down",
  right: "right",
  a: "a",
  b: "b",
  x: "x",
  y: "y",
  start: "s",
  select: "e",
};

function sendKey(command) {
  //if doesn't match the filtered words
  if (!command.match(regexFilter)) {
    let allowKey = true;
    let key = defaultKeyMap[command] || command;
    var parameters = key.split(" ");
    //throttle certain commands (not individually though)
    if (parameters[0].match(regexThrottle)) {
      let newTime = new Date().getTime();
      if (newTime - lastTime[parameters[0]] < config.timeToWait) {
        allowKey = false;
      } else {
        lastTime = newTime;
      }
    }
    if (allowKey) {
      if (isWindows) {
        //use python on windows
        // "VisualBoyAdvance"
        // "DeSmuME 0.9.10 x64"
        //Split Key for Key + nb inputs
        if(parameters.length >= 2) {
          exec("python key.py" + "  \"" + config.programName + "\" " + parameters[0] + " " + parameters[1]);
        } else {
          exec("python key.py" + "  \"" + config.programName + "\" " + parameters[0] + " 1");
        }
      } else {
        //Send to preset window under non-windows systems
        exec(
          "xdotool key --window " +
            windowID +
            " --delay " +
            config.delay +
            " " +
            key
        );
      }
    }
  }
}

exports.sendKey = sendKey;
