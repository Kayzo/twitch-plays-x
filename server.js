const tmi = require("tmi.js");
const keyHandler = require("./keyHandler.js");
const config = require("./config.js");

// https://github.com/tmijs/tmi.js#tmijs
// for more options
const client = new tmi.client({
  connection: {
    secure: true,
    reconnect: true,
  },
  channels: [config.channel],
});

const commandRegex =
  config.regexCommands ||
  new RegExp("^(" + config.commands.join("|") + ")$", "i");

const commandStepsRegex = 
  new RegExp("^(" + config.commands.join("|") + ") [0-9]+$", "i");

client.on("message", function (channel, tags, message, self) {
  let isCorrectChannel = `#${config.channel}` === channel;
  let messageMatches = message.match(commandRegex) || message.match(commandStepsRegex);

  if (self) return;
  if (isCorrectChannel && messageMatches) {
    // print username and message to console
    var messageToDisplay = message.split(" ")
    if(messageToDisplay.length === 1){
      console.log(`${tags.username}: ${messageToDisplay[0]} (1x)`);
      // send the message to the emulator
      keyHandler.sendKey(message.toLowerCase());
    } else {
      if(messageToDisplay[1] <= 10) {
        console.log(`${tags.username}: ${messageToDisplay[0]} (${messageToDisplay[1]}x)`);
        // send the message to the emulator
        keyHandler.sendKey(message.toLowerCase());
      }
    }
  }
});

client.addListener("connected", function (address, port) {
  console.log("Connected! Waiting for messages..");
});
client.addListener("disconnected", function (reason) {
  console.log("Disconnected from the server! Reason: " + reason);
});

client.connect();
if (config.channel === 'twitchplayspokemon') {
  console.log("");
  console.log("'twitchplayspokemon' is the default channel! Otherwise, run with the environment variable: ");
  console.log("TWITCH_CHANNEL=mychannelhere npm start");
  console.log("");
}
console.log(`Connecting to /${config.channel}..`);